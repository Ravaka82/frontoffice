import { Component, AfterViewInit  } from '@angular/core';
import { Router } from '@angular/router';
import * as L from 'leaflet';
import { SignalementserviceService } from '../signalementservice.service';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.scss']
})
export class RechercheComponent implements AfterViewInit {
  map: L.Map;
  //this.route.snapshot.queryParams['nomTypeSignalement'];
  private initMap(): void {
    this.map = L.map('map', {
      center: [ -18.575837, 46.741012 ],
      zoom: 6
    });
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }
  constructor(private signalementService: SignalementserviceService,private route: Router) { }
  ngAfterViewInit(): void {
   this.initMap();
  }
  RechercheGlobal(nomTypeSignalement:string,dateMin:string,dateMax:string,status:number){
    if(this.nomTypeSignalement!= null && this.dateMin!=null && this.dateMax!=null && this.status!=0){
      this.getRechercheparRegion(this.nomTypeSignalement.toString(),this.dateMin.toString(),this.dateMax.toString(),this.status,this.monObjet)
    }
    if(this.nomTypeSignalement!= null && this.dateMin==null && this.dateMax==null && this.status==null){
     this.getRechercheparTypeSignalement(this.nomTypeSignalement.toString(),this.monObjet)
    }
    if(this.nomTypeSignalement== null && this.dateMin!=null && this.dateMax!=null && this.status==null){
     this.getRechercheparDate(this.dateMin.toString(),this.dateMax.toString(),this.monObjet)
    }
    if(this.nomTypeSignalement== null && this.dateMin==null && this.dateMax==null && this.status!=null){
     this.getRechercheparStatus(this.status,this.monObjet)
    }
  }
  getRechercheparRegion(nomTypeSignalement:string,dateMin:string,dateMax:string,status:number,token:string): void{
   console.log(this.dateMin);
    this.signalementService.getRechercheparRegion(this.nomTypeSignalement.toString(),this.dateMin.toString(),this.dateMax.toString(),this.status,this.monObjet)
     .subscribe(
       data => { 
         this.signalement=data;
         console.log(data);
       })
  }
  getRechercheparTypeSignalement( nomTypeSignalement:string,token:string): void{
   this.signalementService.getRechercheparTypeSignalement(this.nomTypeSignalement.toString(),this.monObjet)
   .subscribe(
     data => {
       this.signalement=data;
        console.log(this.signalement[0].positionx);
 
     })
  }
  getRechercheparDate( date1 :string , date2:string,token:string): void{
   this.signalementService.getRechercheparDate(this.dateMin.toString(),this.dateMax.toString(),this.monObjet)
   .subscribe(
     data => {
       this.signalement=data;
    
       console.log(data);
     })
  }
  getRechercheparStatus(status:number,token:string): void{
   this.signalementService.getRechercheparStatus(this.status,this.monObjet)
   .subscribe(
     data => {
       this.signalement=data;
     console.log(data);
     })
  } 

}
