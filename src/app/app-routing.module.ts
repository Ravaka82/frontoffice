import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AcceuilFrontComponent } from './acceuil-front/acceuil-front.component';
import { MapComponent } from './map/map.component';
import { RechercheComponent } from './recherche/recherche.component';

const routes: Routes = [
  {path:'login', component: LoginComponent},
  {path: 'acceuil', component: AcceuilFrontComponent},
  {path: 'MapComponent', component: MapComponent},
  {path:'Recherche',component: RechercheComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
