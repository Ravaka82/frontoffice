import { TestBed } from '@angular/core/testing';

import { SignalementAffecterService } from './signalement-affecter.service';

describe('SignalementAffecterService', () => {
  let service: SignalementAffecterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignalementAffecterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
