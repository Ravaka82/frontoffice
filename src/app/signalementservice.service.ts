import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Signalement } from './Model/Signalement';
import * as L from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class SignalementserviceService {

  constructor(private http:HttpClient){}
  Url1= 'http://localhost:8081/api/getParRegion';
  Url2= 'http://localhost:8081/api/getRechercheParTypeSignalement';
  Url3= 'http://localhost:8081/api/getRechercheParDate';
  Url4= 'http://localhost:8081/api/getRechercheParStatus';

  getRechercheparRegion(nomTypeSignalement:string,date1:string,date2:string,status:number,token:string){
       let queryParams = new HttpParams();
       queryParams = queryParams.append("nomTypeSignalement",nomTypeSignalement);
       queryParams = queryParams.append("date1",date1);
       queryParams = queryParams.append("date2",date2);
       queryParams = queryParams.append("status",status);
       queryParams = queryParams.append("token",token);
       return this.http.get<any>(this.Url1,{params:queryParams});
     }
  getRechercheparTypeSignalement( nomTypeSignalement:string,token:string){
      let queryParams = new HttpParams();
      queryParams = queryParams.append("nomTypeSignalement",nomTypeSignalement);
      queryParams = queryParams.append("token",token);
          return this.http.get<any>(this.Url2,{params:queryParams});
    }
    getRechercheparDate( date1 :string , date2:string,token:string){
      let queryParams = new HttpParams();
      queryParams = queryParams.append("date1",date1);
      queryParams = queryParams.append("date2",date2);
      queryParams = queryParams.append("token",token);
      return this.http.get<any>(this.Url3,{params:queryParams});
    }
    getRechercheparStatus(status:number,token:string){
      let queryParams = new HttpParams();
      queryParams = queryParams.append("status",status);
      queryParams = queryParams.append("token",token);
      return this.http.get<any>(this.Url4,{params:queryParams});
    }
 

}
