
import { Router } from '@angular/router';
import * as L from 'leaflet';
import { Signalement } from '../Model/Signalement';
import { TypeSignalement } from '../Model/TypeSignalement';
import { SignalementserviceService } from '../signalementservice.service';
import { TypeserviceService } from '../typeservice.service';
import { Component, AfterViewInit  } from '@angular/core';


@Component({
  selector: 'app-acceuil-front',
  templateUrl: './acceuil-front.component.html',
  styleUrls: ['./acceuil-front.component.scss']
})
export class AcceuilFrontComponent implements AfterViewInit {
  private map: any;
  typeSignalement : TypeSignalement[];
  signalement: Signalement[];
  dateMin: string;
    dateMax: string;
    nomTypeSignalement: string;
    status: number;
    valiny:void;
    monObjet: any=localStorage.getItem('monObjet');

 
  
    private initMap(): void {
      this.map = L.map('map', {
        center: [ -18.575837, 46.741012 ],
        zoom: 6
      });
      const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        minZoom: 3,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      });
  
      tiles.addTo(this.map);
    }
    constructor(private signalementService: SignalementserviceService,private typeservice: TypeserviceService,private router: Router) { }
    ngAfterViewInit(): void {
      this.listSignalement();
      this.initMap();
      this.RechercheGlobal(this.nomTypeSignalement,this.dateMin,this.dateMax,this.status);
      console.log(this.RechercheGlobal);
   
     }
     listSignalement(): void{
      this.typeservice.getAllTypeSignalement()
        .subscribe(
          data => {
            this.typeSignalement=data;
          })
     }
     RechercheGlobal(nomTypeSignalement:string,dateMin:string,dateMax:string,status:number){
      if(this.nomTypeSignalement!= null && this.dateMin!=null && this.dateMax!=null && this.status!=0){
        this.getRechercheparRegion(this.nomTypeSignalement.toString(),this.dateMin.toString(),this.dateMax.toString(),this.status,this.monObjet)
      }
      if(this.nomTypeSignalement!= null && this.dateMin==null && this.dateMax==null && this.status==null){
       this.getRechercheparTypeSignalement(this.nomTypeSignalement.toString(),this.monObjet)
      }
      if(this.nomTypeSignalement== null && this.dateMin!=null && this.dateMax!=null && this.status==null){
       this.getRechercheparDate(this.dateMin.toString(),this.dateMax.toString(),this.monObjet)
      }
      if(this.nomTypeSignalement== null && this.dateMin==null && this.dateMax==null && this.status!=null){
       this.getRechercheparStatus(this.status,this.monObjet)
      }
    }
    getRechercheparRegion(nomTypeSignalement:string,dateMin:string,dateMax:string,status:number,token:string): void{
     console.log(this.dateMin);
      this.signalementService.getRechercheparRegion(this.nomTypeSignalement.toString(),this.dateMin.toString(),this.dateMax.toString(),this.status,this.monObjet)
       .subscribe(
         data => { 
           this.signalement=data;
           console.log(data);
         })
    }
    getRechercheparTypeSignalement( nomTypeSignalement:string,token:string): void{
     this.signalementService.getRechercheparTypeSignalement(this.nomTypeSignalement.toString(),this.monObjet)
     .subscribe(
       data => {
         this.signalement=data;
          console.log(this.signalement[0].positionx);
   
       })
    }
    getRechercheparDate( date1 :string , date2:string,token:string): void{
     this.signalementService.getRechercheparDate(this.dateMin.toString(),this.dateMax.toString(),this.monObjet)
     .subscribe(
       data => {
         this.signalement=data;
      
         console.log(data);
       })
    }
    getRechercheparStatus(status:number,token:string): void{
     this.signalementService.getRechercheparStatus(this.status,this.monObjet)
     .subscribe(
       data => {
         this.signalement=data;
       console.log(data);
       })
    }
    onSubmit() {
     this.RechercheGlobal(this.nomTypeSignalement,this.dateMin,this.dateMax,this.status);
     //this.valiny=this.RechercheGlobal(this.nomTypeSignalement,this.dateMin,this.dateMax,this.status);
    //  for (let i = 0; i <this.valiny. i++) {
    //    const element = array[i];
       
    //  }
     //console.log("appotra = "+this.valiny);
     this.router.navigate(['/Recherche',{ nomTypeSignalement:this.nomTypeSignalement , dateMin: this.dateMin, dateMax: this.dateMax,status:this.status }]);   
   }
}
