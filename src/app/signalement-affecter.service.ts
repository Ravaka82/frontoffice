import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignalementAffecterService {

  constructor(private http:HttpClient){}
  Url1= 'http://localhost:8081/api/getAfficheSignalement';
 

  getAfficheSignalement(nomtypeSignalement:string,token:string){
    let queryParams = new HttpParams();
    queryParams = queryParams.append("nomtypeSignalement",nomtypeSignalement);
    queryParams = queryParams.append("token",token);
    return this.http.get<any>(this.Url1,{params:queryParams});
  }

}
