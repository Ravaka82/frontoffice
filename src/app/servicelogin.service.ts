import { Injectable } from '@angular/core';
import { HttpParams} from '@angular/common/http';
import { HttpClient,HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceloginService {
  private url="http://localhost:8081/api/LoginControlleRegion";

  constructor(private http: HttpClient) { }
  
  loginRegion(nomRegion:string,mdp:string){
    let params = new HttpParams();
    params = params.set('nomRegion', nomRegion);
    params = params.set('mdp', mdp);
    return this.http.post(this.url, params,{responseType: 'text'});
  }

}
