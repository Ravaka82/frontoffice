import { Component, OnInit } from '@angular/core';
import {Region} from '../Model/Region';
import {ServiceloginService} from '../servicelogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  Region: Region[] ;
  region: Region = new Region();
  message: string;
  constructor(private serviceloginService : ServiceloginService,private router: Router) {}

  ngOnInit(): void {
  }
  LoginControle()
  {
   this.serviceloginService.loginRegion(this.region.nomRegion,this.region.mdp)
   .subscribe(
     (data:string) => {
       console.log(data);
      if(data!='Erreur'){
         this.router.navigate(['/acceuil']);
         localStorage.setItem('monObjet', data);
         
         sessionStorage.setItem('clé', this.region.nomRegion);
         
       }
       else{
         this.router.navigate(['/login']);
         this.message="Erreur"; 
       }
     })
   }

  onSubmit() {
   this.LoginControle();
 }
}
