import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { TypeSignalement } from './Model/TypeSignalement';


@Injectable({
  providedIn: 'root'
})
export class TypeserviceService {

  constructor(private http:HttpClient){}
  Url2= 'http://localhost:8081/api/typeSignalement';

  getAllTypeSignalement()
  {
    return this.http.get<TypeSignalement[]>(this.Url2);
  }
}

