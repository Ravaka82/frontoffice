import { Component, AfterViewInit  } from '@angular/core';
import { AcceuilFrontComponent } from '../acceuil-front/acceuil-front.component';
import * as L from 'leaflet';
import { fromEvent } from 'rxjs';
import { SignalementserviceService } from '../signalementservice.service';
import { Signalement } from '../Model/Signalement';
import { getCurrencySymbol, NumberSymbol } from '@angular/common';
import { SignalementAffecterService } from '../signalement-affecter.service';
import { tokenize } from '@angular/compiler/src/ml_parser/lexer';
import { TypeserviceService } from '../typeservice.service';
import { TypeSignalement } from '../Model/TypeSignalement';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit  {
  map: L.Map;
signalement: Signalement[];
dateMin: string;
typesignalement: TypeSignalement[];
  dateMax: string;
  nomtypeSignalement: string='Dechets';
  
  monObjet: any=localStorage.getItem('monObjet');

  private initMap(): void {
    this.map = L.map('map', {
      center: [ -18.575837, 46.741012 ],
      zoom: 6
    });
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }
  constructor(private signalementaffect:SignalementAffecterService,private typesignal:TypeserviceService) { }
  ngAfterViewInit(): void {
    
    this.initMap();
    // var greenIcon = new L.Icon({
    //   iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
    //   shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    //   iconSize: [25, 41],
    //   iconAnchor: [12, 41],
    //   popupAnchor: [1, -34],
    //   shadowSize: [41, 41]
     
    // });
    

    this.typesignal.getAllTypeSignalement()
    .subscribe(
      data => {
        this.typesignalement=data;
        for (let j = 0; j < this.typesignalement.length; j++) {
          // const element = array[j];
          this.signalementaffect.getAfficheSignalement(this.typesignalement[j].nomTypeSignalement,this.monObjet)
   .subscribe(
     data => {
       this.signalement=data;
       for (let i = 0; i < this.signalement.length; i++) {
        console.log("ravaka="+this.signalement[i].positionx); 
         const longitude=this.signalement[i].positionx; 
          const latitude =this.signalement[i].positiony;
          
          const marker = L.circleMarker([longitude,latitude]).
            bindPopup(this.typesignalement[j].nomTypeSignalement).openPopup();
            marker.setStyle({ fillColor:this.signalement[j].typeSignalement.nomcouleur});
            console.log(this.signalement[j].typeSignalement.nomcouleur);
          marker.addTo(this.map);
          
       } 
     })
        }
      })
    
   }
   
}
