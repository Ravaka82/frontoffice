import { Region } from "./Region";
import { Signalement } from "./Signalement";

export class SignalementAffecter{
    idsignalementaffecter: number;
    signalement: Signalement;
    region: Region;
    status: number;
}