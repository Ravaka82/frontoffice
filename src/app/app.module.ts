import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { AcceuilFrontComponent } from './acceuil-front/acceuil-front.component';
import { MapComponent } from './map/map.component';
import { RechercheComponent } from './recherche/recherche.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AcceuilFrontComponent,
    MapComponent,
    RechercheComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [MapComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
