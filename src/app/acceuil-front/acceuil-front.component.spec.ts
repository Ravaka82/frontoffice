import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceuilFrontComponent } from './acceuil-front.component';

describe('AcceuilFrontComponent', () => {
  let component: AcceuilFrontComponent;
  let fixture: ComponentFixture<AcceuilFrontComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AcceuilFrontComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceuilFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
