import { Region } from "./Region";
import { TypeSignalement } from "./TypeSignalement";
import { Utilisateur } from "./Utilisateur";

export class Signalement{
    idsignalement: number;
    utilisateur:Utilisateur;
    region:Region;
    dateSignalement: Date;
    description : String ;
    typeSignalement: TypeSignalement;
    positionx: number;
    positiony: number;
    status: number;
 
    
 }